<style>
/* 100 Bytes of CSS to look good nearly anywhere */
body {
  max-width: 70ch;
  padding: 3em 1em;
  margin: auto;
  line-height: 1.75;
  font-size: 1.25em;
  font-family: sans-serif;
}

/* 100 More optional bytes */
h1, h2, h3, h4, h5, h6 {
  text-align: center;
  margin: 1em 0 1em;
}

p, ul, ol {
  margin-bottom: 2em;
  color: #1d1d1d;
}

/* Don't underline the link to the index page */
h1 a {
  text-decoration: none;
  color: black;
  font-weight: bold;
}

/* Clear around images, and preallocate their space */
img {
  display: block;
  margin: 2 auto;
  aspect-ratio: attr(width) / attr(height);
}

/* For each particular image, set its size */
img[alt="A diagram showing the difference in pipeline between normal websites with a CDN and this monstrosity"] {
  width: 582px;
  height: 521px;
}
</style>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Markdown@Edge</title>
<a href="/"><h1>Markdown@Edge</h1></a>

---

