# About the Author

This very silly application was written by Leonora Tindall.
You can find her other work at [nora.codes](https://nora.codes),
or find her on [Twitter](https://twitter.com/NoraDotCodes)
and on the Fediverse at [Weirder Earth](https://weirder.earth/@noracodes).


Thanks for visiting!
