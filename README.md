# Markdown@Edge

A Markdown renderer on the Compute@Edge platform from Fastly.

The main source file, `src/main.rs`, contains all the code for the renderer.
The content on the server is stored in `content/` for convenience.

## Deploying Content

Simply use `scp` to copy the files to the server:

```
scp -r ./content/* example.com:/var/www-html/edgeblog/
```

## Nginx Configuration

In order to best support this application, it's important to add the following
line to Nginx's MIME types configuration:

```text
text/markdown     md;
```

I also disabled gzip and set the max-age and cache-control headers as follows:

```nginx
server {
  # ...
  location /edgeblog {
    # To illustrate the source to visitors.
    autoindex on;
    autoindex_exact_size off;
    autoindex_format html;
    autoindex_localtime on;

    # To facilitate tagging.
    gzip off;

    # Cache-Control headers
    expires 1h;
    add_header Cache-Control "public";
  }
}
```
